# -*- coding: utf-8 -*-
import unittest
import requests
import json
import xmlrunner
import sys

class InequalityTest(unittest.TestCase):

  urlp2 = ""
    
  def Ontobi_API(self, S1,pos):
        url = 'https://api.ontobi.com/' + self.urlp2 + '/fid'
        payload = {"msgId":"hello","bodyS1":S1}
        headers = {'content-type': 'application/json'}

        session = requests.Session()

        r = session.post(url, data = json.dumps(payload), headers = headers)
        j = r.json()

        output = str(j["data"]["tokenInfo"][pos]["partOfSpeechProperty"])

        return output
        
  def Ontobi_API_crf(self, S1,pos):
        url = 'https://api.ontobi.com/' + self.urlp2 + '/preprocess2'
        payload = {"msgId":"hello","bodyS1":S1}
        headers = {'content-type': 'application/json'}

        session = requests.Session()

        r = session.post(url, data = json.dumps(payload), headers = headers)
        j = r.json()

        url = 'https://api.ontobi.com/dev/crf'
        payload2 ={'msgId':u'test',u'tokenInfo':j['data']['tokenInfo'],}

        session2 = requests.Session()

        s = session2.post(url, data = json.dumps(payload2), headers = headers)
        k = s.json()

        output = str(k["tokenInfo"][pos]["posLongStr"])

        return output  
  
  def test_compareCrfToPos_1(self):
      text     = "The dog is cute."
      Actual1=self.Ontobi_API(text,0)
      Actual2=self.Ontobi_API_crf(text,0)
      self.assertEqual(Actual1, Actual2)
      
  def test_compareCrfToPos_2(self):
      text     = "The dog is cute."
      Actual1=self.Ontobi_API(text,1)
      Actual2=self.Ontobi_API_crf(text,1)
      self.assertEqual(Actual1, Actual2)
      
  def test_compareCrfToPos_3(self):
      text     = "The dog is cute."
      Actual1=self.Ontobi_API(text,2)
      Actual2=self.Ontobi_API_crf(text,2)
      self.assertEqual(Actual1, Actual2)

  def test_compareCrfToPos_4(self):
      text     = "The dog is cute."
      Actual1=self.Ontobi_API(text,3)
      Actual2=self.Ontobi_API_crf(text,3)
      self.assertEqual(Actual1, Actual2)
     
  def test_compareCrfToPos_5(self):
      text     = "Hillary Clinton Struggles to Find Footing in Unusual Race."
      Actual1=self.Ontobi_API(text,1)
      Actual2=self.Ontobi_API_crf(text,1)
      self.assertEqual(Actual1, Actual2)
     
  def test_compareCrfToPos_6(self):
      text     = "Donald Trump and Bikers Share Affection at Rolling Thunder Rally."
      Actual1=self.Ontobi_API(text,2)
      Actual2=self.Ontobi_API_crf(text,2)
      self.assertEqual(Actual1, Actual2)
     
  def test_compareCrfToPos_7(self):
      text     = "Gorilla Killed After Child Enters Enclosure at Cincinnati Zoo."
      Actual1=self.Ontobi_API(text,3)
      Actual2=self.Ontobi_API_crf(text,3)
      self.assertEqual(Actual1, Actual2)
     
  def test_compareCrfToPos_8(self):
      text     = "Hillary Clinton Says Donald Trump Is Not Qualified to Be President."
      Actual1=self.Ontobi_API(text,4)
      Actual2=self.Ontobi_API_crf(text,4)
      self.assertEqual(Actual1, Actual2)
     
  def test_compareCrfToPos_9(self):
      text     = "Donald Trump Brings Up 1999 Rape Allegation Against Bill Clinton."
      Actual1=self.Ontobi_API(text,4)
      Actual2=self.Ontobi_API_crf(text,4)
      self.assertEqual(Actual1, Actual2)
     
  def test_compareCrfToPos_10(self):
      text     = "Success of Jerry Brown, and California, Offers Lesson to National Democrats."
      Actual1=self.Ontobi_API(text,4)
      Actual2=self.Ontobi_API_crf(text,4)
      self.assertEqual(Actual1, Actual2)
     
  def test_compareCrfToPos_11(self):
      text     = "Libertarians See Chance Amid Discontent Over Donald Trump and Hillary Clinton."
      Actual1=self.Ontobi_API(text,5)
      Actual2=self.Ontobi_API_crf(text,5)
      self.assertEqual(Actual1, Actual2)
     
  def test_compareCrfToPos_12(self):
      text     = "Rise of Donald Trump Tracks Growing Debate Over Global Fascism."
      Actual1=self.Ontobi_API(text,6)
      Actual2=self.Ontobi_API_crf(text,6)
      self.assertEqual(Actual1, Actual2)
     
  def test_compareCrfToPos_13(self):
      text     = "California Looms as Delegate Prize, and as One More Democratic Battlefield."
      Actual1=self.Ontobi_API(text,6)
      Actual2=self.Ontobi_API_crf(text,6)
      self.assertEqual(Actual1, Actual2)
     
  def test_compareCrfToPos_14(self):
      text     = "Kamala Harris, a ‘Top Cop’ in the Era of Black Lives Matter."
      Actual1=self.Ontobi_API(text,7)
      Actual2=self.Ontobi_API_crf(text,7)
      self.assertEqual(Actual1, Actual2)
     
  def test_compareCrfToPos_15(self):
      text     = "Three Days, 700 Deaths on Mediterranean as Migrant Crisis Flares."
      Actual1=self.Ontobi_API(text,2)
      Actual2=self.Ontobi_API_crf(text,2)
      self.assertEqual(Actual1, Actual2)
     
if __name__ == '__main__':
    InequalityTest.urlp2 = sys.argv[1]
    suite = unittest.TestLoader().loadTestsFromTestCase(InequalityTest)
    xmlrunner.XMLTestRunner(output='test-reports3').run(suite)